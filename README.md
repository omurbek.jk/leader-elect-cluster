leader-elect-cluster
==============


## Description
A good base for a highly available self healing cluster with leader election. This base will install a file in /etc/profile.d/cluster that can be sourced in in order to define some necessary information for initial leader election.

In the boot scripts of any project extending this one, if access is needed to the information about the cluster, simply source in the /etc/profile.d/cluster file as follows:
```
source /etc/profile.d/cluster
```
and the script will have access to the following variables:

`IS&#95;LEADER` has possible values of [ "true" | "false" ] and tells the script if this instance is the leader in the cluster or not.

`CLUSTER&#95;ADDRESSES` sets a value as a csv like: "1.2.3.4 , 3.4.5.6". This is all the ip addresses of the running instances in the cluster.



## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/leader-elect-cluster/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

### `DNS_ZONE`:
  * description: the zone in which the internal elb dns entry should be maintained

### `CLUSTER_AMI`:
  * description: the ami to launch for the cluster - default is Amazon Linux AMI 2015.03 (HVM), SSD Volume Type


## Required variables with default

### `VPC_NAME`:
  * description: The cloudcoreo defined vpc to add this cluster to
  * default: dev-vpc

### `VPC_CIDR`:
  * description: the cloudcoreo defined vpc to add this cluster to
  * default: 10.1.0.0/16

### `PRIVATE_SUBNET_NAME`:
  * description: the private subnet in which the cluster should be added
  * default: dev-private-subnet

### `PRIVATE_ROUTE_NAME`:
  * description: the private subnet in which the cluster should be added
  * default: dev-private-route

### `CLUSTER_NAME`:
  * description: the name of the cluster - this will become your dns record too
  * default: leadercluster

### `CLUSTER_ELB_TRAFFIC_PORTS`:
  * description: ports that need to allow traffic into the ELB
  * default: 50

### `CLUSTER_ELB_TRAFFIC_CIDRS`:
  * description: the cidrs to allow traffic from on the ELB itself
  * default: 10.0.0.0/8

### `CLUSTER_TCP_HEALTH_CHECK_PORT`:
  * description: a tcp port the ELB will check every so often - this defines health and ASG termination
  * default: 8001

### `CLUSTER_INSTANCE_TRAFFIC_PORTS`:
  * description: ports to allow traffic on directly to the instances
  * default: 20fb, 1f41, 1f42, 16

### `CLUSTER_INSTANCE_TRAFFIC_CIDRS`:
  * description: cidrs that are allowed to access the instances directly
  * default: 10.0.0.0/8

### `CLUSTER_SIZE`:
  * description: the image size to launch
  * default: t2.small


### `CLUSTER_GROUP_SIZE_MIN`:
  * description: the minimum number of instances to launch
  * default: 3

### `CLUSTER_GROUP_SIZE_MAX`:
  * description: the maxmium number of instances to launch
  * default: 5

### `CLUSTER_HEALTH_CHECK_GRACE_PERIOD`:
  * description: the time in seconds to allow for instance to boot before checking health
  * default: 600

### `CLUSTER_UPGRADE_COOLDOWN`:
  * description: the time in seconds between rolling instances during an upgrade
  * default: 300


## Optional variables with default

### `ELB_LISTENERS`:
  * description: The listeners to apply to the ELB
  * default: 
```
[
  {
    :elb_protocol => 'http', 
    :elb_port => 80,
    :to_protocol => 'https', 
    :to_port => 8443
  }
]

```

### `WAIT_FOR_CLUSTER_MIN`:
  * description: true if the cluster should wait for all instances to be in a running state
  * default: true


## Optional variables with no default

### `VPC_SEARCH_TAGS`:
  * description: If you have more than one VPC with the same CIDR, and it is not under CloudCoreo control, we need a way to find it. Enter some unique tags that exist on the VPC you want us to find. ['env=production','Name=prod-vpc']

### `PRIVATE_ROUTE_SEARCH_TAGS`:
  * description: If you more than one route table or set of route tables, and it is not under CloudCoreo control, we need a way to find it. Enter some unique tags that exist on your route tables you want us to find. i.e. ['Name=my-private-routetable','env=dev']

### `PRIVATE_SUBNET_SEARCH_TAGS`:
  * description: Usually the private-routetable association is enough for us to find the subnets you need, but if you have more than one subnet, we may need a way to find them. unique tags is a great way. enter them there. i.e. ['Name=my-private-subnet']

### `CLUSTER_KEY`:
  * description: the ssh key to associate with the instance(s) - blank will disable ssh

## Tags
1. Base
1. Leader Election
1. High Availability

## Categories
1. Servers



## Diagram
![diagram](https://raw.githubusercontent.com/CloudCoreo/leader-elect-cluster/master/images/diagram.gif "diagram")


## Icon
![icon](https://raw.githubusercontent.com/CloudCoreo/leader-elect-cluster/master/images/icon.png "Cluster Icon")


