#!/bin/bash
######################################################################
##
## Required Variables:
##   CLUSTER_GROUP_SIZE_MIN
##
## Optional Variables:
##   WAIT_FOR_CLUSTER_MIN
##
######################################################################
##
## Required Packages
##   aws-cli in pip
##
######################################################################
set -x

work_dir='/var/tmp/cluster'

mkdir -p "$work_dir"

region="$(curl -sL 169.254.169.254/latest/meta-data/placement/availability-zone | sed '$s/.$//')"

addresses="$(python ./lib/group_addresses.py)"
## lets wait until the minimum actually exists
if [ -n "${WAIT_FOR_CLUSTER_MIN:-}" ]; then
    while [ "$(wc -l < <(echo $addresses | perl -pe 's{\s}{\n}g'))" -lt "${CLUSTER_GROUP_SIZE_MIN:-0}" ]; do
	sleep 1
	addresses="$(python ./lib/group_addresses.py)"
    done
fi

leader_ip="$(echo $addresses | perl -pe 's{\s}{\n}g' | head -1)"
my_ipaddress="$(curl -sL 169.254.169.254/latest/meta-data/local-ipv4)"
is_leader="false"
if [ "$my_ipaddress" = "$leader_ip" ]; then
    is_leader="true"
fi

joinArr=""
for ip in $addresses; do
    ## don't join myself
    if [ "$ip" != "$my_ipaddress" ]; then
	if [ -n "$joinArr" ]; then
	    joinArr="$joinArr ,"
	fi
	joinArr="$joinArr $ip"
    fi
done

profile_script='/etc/profile.d/cluster'

cat <<EOF > "$profile_script"
#!/bin/bash

## IS_LEADER possible values are: [ "true" | "false" ]
## CLUSTER_ADDRESSES value is a csv like: "1.2.3.4 , 3.4.5.6"

export MY_IPADDRESS="$my_ipaddress"
export CLUSTER_ADDRESSES="$joinArr"
export IS_LEADER="$is_leader"

EOF
