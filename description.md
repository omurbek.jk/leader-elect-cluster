A good base for a highly available self healing cluster with leader election. This base will install a file in /etc/profile.d/cluster that can be sourced in in order to define some necessary information for initial leader election.

In the boot scripts of any project extending this one, if access is needed to the information about the cluster, simply source in the /etc/profile.d/cluster file as follows:
```
source /etc/profile.d/cluster
```
and the script will have access to the following variables:

`IS&#95;LEADER` has possible values of [ "true" | "false" ] and tells the script if this instance is the leader in the cluster or not.

`CLUSTER&#95;ADDRESSES` sets a value as a csv like: "1.2.3.4 , 3.4.5.6". This is all the ip addresses of the running instances in the cluster.

